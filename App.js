import React from 'react';
import { View, StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { GlobalProvider } from "./context/GlobalState";
import { Home } from "./components/Home";
import { AddNote } from "./components/AddNote";
import { EditNote } from "./components/EditNote";
import { NoteList } from "./components/NoteList";

const Stack = createStackNavigator();

const App = () => {
	return (
		<GlobalProvider>
			<NavigationContainer>
				<Stack.Navigator>
					<Stack.Screen name="Home" component={Home} />
					<Stack.Screen name="Adicionar" component={AddNote} />
					<Stack.Screen name="Editar" component={EditNote} />
					<Stack.Screen name="Listar" component={NoteList} />
				</Stack.Navigator>
			</NavigationContainer>
		</GlobalProvider>
	);
}

export default App;