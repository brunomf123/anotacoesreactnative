export default (state, action) => {
	switch (action.type) {
		case 'REMOVE_NOTE':
			return {
				...state,
				notes: state.notes.filter(note => {
					return note.id !== action.payload;
				})
			}
		case 'ADD_NOTE':
			return {
				...state,
				notes: [action.payload, ...state.notes]
			}
		case 'EDIT_NOTE':
			const updateNote = action.payload;

			const updateNotes = state.notes.map(note => {
				if (note.id === updateNote.id) {
					return updateNote;
				}
				return note;
			})
			return {
				...state,
				notes: updateNotes
			}

		default:
			return state;
	}
}