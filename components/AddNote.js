import React, { useState, useContext } from 'react';
import { GlobalContext } from "../context/GlobalState";
import { StyleSheet, View, Text, TextInput, Button } from 'react-native';

export const AddNote = ({ navigation }) => {
	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');
	const { addNote, last_id } = useContext(GlobalContext);

	const onSubmit = () => {
		const newNote = {
			id: last_id + 1,
			title,
			description
		}
		addNote(newNote);
		navigation.navigate('Home');
	}

	const onChangeTitle = (e) => {
		setTitle(e);
	}

	const onChangeDescription = (e) => {
		setDescription(e);
	}

	return (
		<View style={styles.container}>
			<View>
				<View style={styles.cell}>
					<Text style={styles.title}>TÍTULO:</Text>
					<TextInput value={title} onChangeText={onChangeTitle} placeholder="Informe o título..."></TextInput>
				</View>
				<View style={styles.cell}>
					<Text style={styles.title}>ANOTAÇÃO:</Text>
					<TextInput multiline numberOfLines={4} value={description} onChangeText={onChangeDescription} placeholder="Informe a anotação..."></TextInput>
				</View>
				<View style={styles.cell}>
					<View style={styles.row}>
						<View style={styles.subtitle, styles.button}>
							<Button
								title='REGISTRAR'
								onPress={() => onSubmit()}>
							</Button>
						</View>
					</View>
				</View>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
	textCenter: {
		textAlign: 'center',
	},
	margintop: {
		marginTop: '1%',
	},
	title: {
		fontWeight: 'bold',
	},
	subtitle: {
		paddingLeft: 2,
		paddingRight: 2,
	},
	row: {
		flexDirection: "row",
		flexWrap: "wrap",
	},
	button: {
		width: 250,
	},
	cell: {
		width: 250,
		marginTop: '2%',
	}
});

export default AddNote;
