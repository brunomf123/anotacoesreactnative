import React, { useState, useContext, useEffect } from 'react';
import { GlobalContext } from "../context/GlobalState";
import { StyleSheet, View, Text, TextInput, Button } from 'react-native';

export const EditNote = ({ route, navigation }) => {
	const { editNote, notes } = useContext(GlobalContext);
	const [selectedNote, setSelectedNote] = useState({
		id: '',
		title: '',
		description: ''
	})

	const currentNoteId = route.params.id;

	useEffect(() => {
		const noteId = currentNoteId;
		const selectedNote = notes.find(note => note.id === noteId);
		setSelectedNote(selectedNote);
	}, [currentNoteId, notes])

	const onChangeTitle = (e) => {
		setSelectedNote({ ...selectedNote, ['title']: e })
	}

	const onChangeDescription = (e) => {
		setSelectedNote({ ...selectedNote, ['description']: e })
	}

	const onSubmit = (e) => {
		editNote(selectedNote);
		navigation.navigate('Home');
	}

	return (
		<View style={styles.container}>
			<View>
				<View style={styles.cell}>
					<Text style={styles.title}>TÍTULO:</Text>
					<TextInput value={selectedNote.title} onChangeText={onChangeTitle} placeholder="Informe o título..."></TextInput>
				</View>
				<View style={styles.cell}>
					<Text style={styles.title}>ANOTAÇÃO:</Text>
					<TextInput multiline numberOfLines={4} value={selectedNote.description} onChangeText={onChangeDescription} placeholder="Informe a anotação..."></TextInput>
				</View>
				<View style={styles.cell}>
					<View style={styles.row}>
						<View style={styles.subtitle, styles.button}>
							<Button
								color='green'
								title='ALTERAR'
								onPress={() => onSubmit()}>
							</Button>
						</View>
					</View>
				</View>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
	textCenter: {
		textAlign: 'center',
	},
	margintop: {
		marginTop: '1%',
	},
	title: {
		fontWeight: 'bold',
	},
	subtitle: {
		paddingLeft: 2,
		paddingRight: 2,
	},
	row: {
		flexDirection: "row",
		flexWrap: "wrap",
	},
	button: {
		width: 250,
	},
	cell: {
		width: 250,
		marginTop: '2%',
	}
});

export default EditNote;
