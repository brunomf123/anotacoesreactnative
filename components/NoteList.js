import React, { useContext } from 'react';
import { GlobalContext } from '../context/GlobalState';
import { StyleSheet, View, Text, Button } from 'react-native';

export const NoteList = ({ navigation }) => {
	const { notes, removeNote } = useContext(GlobalContext);

	return (
		<View style={styles.container}>
			{notes.length > 0 ? (
				<>
					{notes.map((note) => (
						<View style={styles.marginbottom} key={note.id}>
							<View style={styles.cell}>
								<Text style={styles.title}>TÍTULO:</Text>
								<Text style={styles.subtitle}>{note.title}</Text>
							</View>
							<View style={styles.cell}>
								<Text style={styles.title}>ANOTAÇÃO:</Text>
								<Text style={styles.subtitle}>{note.description}</Text>
							</View>
							<View style={styles.cell}>
								<View style={styles.row}>
									<View style={styles.subtitle, styles.button}>
										<Button
											color="orange"
											title='EDITAR'
											onPress={() => navigation.navigate('Editar', { id: note.id })}>
										</Button>
									</View>
									<View style={styles.subtitle, styles.button}>
										<Button
											color="red"
											title='EXCLUIR'
											onPress={() => removeNote(note.id)}>
										</Button>
									</View>
								</View>
							</View>
						</View>
					))}
				</>
			) : (
				<Text style={styles.title}>Não há anotações registradas...</Text>
			)}
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
	title: {
		fontWeight: 'bold',
	},
	subtitle: {
		paddingLeft: 2,
		paddingRight: 2,
	},
	row: {
		flexDirection: "row",
		flexWrap: "wrap",
	},
	button: {
		width: 125,
	},
	cell: {
		width: 250,
	},
	marginbottom: {
		marginBottom: '5%',
	}
});

export default NoteList;
