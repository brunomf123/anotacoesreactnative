import React from 'react';
import { StyleSheet, View, Text, Button } from 'react-native';

export const Home = ({ navigation }) => {
	return (
		<View style={styles.container}>
			<View style={styles.row}>
				<View style={styles.button}>
					<Button
						title='ADICIONAR ANOTAÇÃO'
						onPress={() => navigation.navigate('Adicionar')}>
					</Button>
					<Text style={styles.title}>MINHAS ANOTAÇÕES</Text>
				</View>
			</View>

			<View style={styles.marginbottom}>
				<Button
					title='VER ANOTAÇÕES'
					onPress={() => navigation.navigate('Listar')}>
				</Button>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	marginbottom: {
		marginTop: '5%'
	},
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
	title: {
		marginTop: '5%',
		fontWeight: 'bold',
		textAlign: 'center',
	},
	row: {
		flexDirection: "row",
		flexWrap: "wrap",
	},
	button: {
		paddingHorizontal: 8,
		paddingVertical: 6,
		borderRadius: 4,
		backgroundColor: "oldlace",
		alignSelf: "flex-start",
		marginHorizontal: "1%",
		marginBottom: 6,
		minWidth: "48%",
		textAlign: "center",
	},
});

export default Home;